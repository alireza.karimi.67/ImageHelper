package imageenum;

/**
 * Created by alireza on 7/1/15.
 */
public enum  ImageQulity {
    MAX(100),HEIGH(80),MEDIUM(60),LOW(40);
    private final int value;
    private ImageQulity(int value)
    {
        this.value=value;
    }
    public int getValue()
    {
        return value;
    }

}
