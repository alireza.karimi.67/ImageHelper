package imageenum;

/**
 * Created by alireza on 7/2/15.
 */
public enum ImgeType {
    BITMAP(1),BYTE(2),BASE64(3);
    private final int value;
    private ImgeType(int value)
    {
        this.value=value;
    }
    public int getValue()
    {
        return value;
    }

}
