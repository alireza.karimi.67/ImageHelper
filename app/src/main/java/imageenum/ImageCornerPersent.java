package imageenum;

/**
 * Created by alireza on 7/1/15.
 */
public enum ImageCornerPersent {
    P01(1),P02(2),P03(3),P04(4),P05(5),P06(6),P07(7),P08(8),P09(9),P10(10),p20(20),p30(30),p40(40),p50(50),p60(60),p70(70),p80(80),p90(90);
    private final int value;
    private ImageCornerPersent(int value)
    {
        this.value=value;
    }
    public int getValue()
    {
        return value;
    }

}
