package convertorNew;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import imageenum.ImageQulity;

/**
 * Created by alireza on 7/11/15.
 */
public class ConvertorByte {
    private final String TAG="Convertor Byte ";
    private byte[] imageByte;

    public ConvertorByte(byte[] imageByte) {
        this.imageByte = imageByte;
    }

    public String ConvertToBase64(Bitmap.CompressFormat compressFormat, ImageQulity imageQulity)
    {
        Bitmap objectBitmap=generateBitmap();
        ObjectConvertor core=new ObjectConvertor(objectBitmap);
        return core.convertToBase64(compressFormat,imageQulity);
    }

    public Bitmap ConvertToBitmap()
    {
        return generateBitmap();
    }

    public byte[] ConvertToByte()
    {
        return imageByte;
    }

    protected Bitmap generateBitmap()
    {
            return BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length);
    }

}
