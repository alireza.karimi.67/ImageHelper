package convertorNew;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import imageenum.ImageQulity;

import java.io.ByteArrayOutputStream;

/**
 * Created by alireza on 7/12/15.
 */
public class ConvertorFile {
    private final String TAG="Convertor File";
    private String address="";
    private Bitmap imageBitmap;

    public ConvertorFile(String address) {
        this.address = address;
        generateBitmap();
    }

    public String convertToBase64(Bitmap.CompressFormat compressFormat, ImageQulity imageQulity)
    {
        ObjectConvertor core=new ObjectConvertor(imageBitmap);
        return core.convertToBase64(compressFormat,imageQulity);

    }

    public Bitmap convertToBitmap()
    {
        return imageBitmap;
    }

    public byte[] convertToByte(Bitmap.CompressFormat compressFormat, ImageQulity imageQulity)
    {
        Bitmap myBitmap = imageBitmap;
        ByteArrayOutputStream bArrOutStrm = new ByteArrayOutputStream();
        myBitmap.compress(compressFormat, imageQulity.getValue(), bArrOutStrm);
        return bArrOutStrm.toByteArray();

    }


    private void generateBitmap()
    {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        imageBitmap = BitmapFactory.decodeFile(address, options);

    }

}
