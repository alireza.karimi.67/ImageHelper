package convertorNew;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import imageenum.ImageQulity;

/**
 * Created by alireza on 7/11/15.
 */
public class ConvertorBase64 {
    private String imageBase64;
    private final String TAG="Convertor Base64 -> ";
    public ConvertorBase64(String imageBase64) {
        this.imageBase64 = imageBase64;
    }

    public String convertToBase64()
    {
        return imageBase64;
    }

    public byte[] convertToByte(Bitmap.CompressFormat compressFormat, ImageQulity imageQulity)
    {
        Bitmap objectBitmap=generateBitmap();
        ObjectConvertor core=new ObjectConvertor(objectBitmap);
        return core.convertToByte(compressFormat,imageQulity);
    }

    public Bitmap convertToBitmap()
    {
        return generateBitmap();
    }

    protected Bitmap generateBitmap()
    {
            byte[] imgByte = generateByte();
            return BitmapFactory.decodeByteArray(imgByte, 0, imgByte.length);
    }

    protected byte[] generateByte()
    {
            return Base64.decode(imageBase64, 0);
    }

}
