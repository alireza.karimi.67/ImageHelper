package convertorNew;

import android.graphics.Bitmap;
import android.util.Base64;
import android.util.Log;
import imageenum.ImageQulity;

import java.io.ByteArrayOutputStream;

/**
 * Created by alireza on 7/11/15.
 */
public class ConvertorBitmap {
    private Bitmap imageBitmap;
    private final String TAG="Convertor Bitmap";
    public ConvertorBitmap(Bitmap imageBitmap) {
        this.imageBitmap = imageBitmap;
    }

    public String convertToBase64(Bitmap.CompressFormat compressFormat, ImageQulity imageQulity)
    {
        try {
            Bitmap myBitmap = imageBitmap;
            ByteArrayOutputStream bArrOutStrm = new ByteArrayOutputStream();
            myBitmap.compress(compressFormat, imageQulity.getValue(), bArrOutStrm);
            byte[] myByte = bArrOutStrm.toByteArray();
            String myBase64 = Base64.encodeToString(myByte, Base64.DEFAULT);
            return myBase64;
        }catch (Exception ex)
        {
            Log.e(TAG, " An Error Occure -> " + ex.toString());
            return null;
        }
    }

    public byte[] convertToByte(Bitmap.CompressFormat compressFormat, ImageQulity imageQulity)
    {
            Bitmap myBitmap = imageBitmap;
            ByteArrayOutputStream bArrOutStrm = new ByteArrayOutputStream();
            myBitmap.compress(compressFormat, imageQulity.getValue(), bArrOutStrm);
            return bArrOutStrm.toByteArray();
    }

    public Bitmap convertToBitmap()
    {
        return imageBitmap;
    }

}
