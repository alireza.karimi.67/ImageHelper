package convertorNew;

import android.graphics.Bitmap;
import android.util.Base64;
import imageenum.ImageQulity;

import java.io.ByteArrayOutputStream;

/**
 * Created by alireza on 7/11/15.
 */
public class ObjectConvertor {
    private final String TAG="Convertor Core";
    private Bitmap imageBitmap;
    public ObjectConvertor(Bitmap image) {
        imageBitmap=image;
    }

    public String convertToBase64(Bitmap.CompressFormat compressFormat, ImageQulity imageQulity)
    {
            Bitmap myBitmap = imageBitmap;
            ByteArrayOutputStream bArrOutStrm = new ByteArrayOutputStream();
            myBitmap.compress(compressFormat, imageQulity.getValue(), bArrOutStrm);
            byte[] myByte = bArrOutStrm.toByteArray();
            String myBase64 = Base64.encodeToString(myByte, Base64.DEFAULT);
            return myBase64;
    }

    public byte[] convertToByte(Bitmap.CompressFormat compressFormat, ImageQulity imageQulity)
    {
            Bitmap myBitmap = imageBitmap;
            ByteArrayOutputStream bArrOutStrm = new ByteArrayOutputStream();
            myBitmap.compress(compressFormat, imageQulity.getValue(), bArrOutStrm);
            return bArrOutStrm.toByteArray();
    }


}
