package myinterface;

import android.graphics.Bitmap;
import imageenum.ImageCornerPersent;
import imageenum.ImageQulity;
import ir.alirezakarimi.imagehelper.app.MyImage;

/**
 * Created by alireza on 7/11/15.
 */
public interface InterfaceMyImage {
    public MyImage creatTreeSideImage(boolean isAntiAlias);
    public MyImage creatSixSideImage(boolean isAntiAlias);
    public MyImage creatFiveSideImage(boolean isAntiAlias);
    public MyImage creatEightSideImage(boolean isAntiAlias);
    public MyImage creatCirculeImage(boolean isAntiAlias);
    public MyImage creatTreeStrokSideImage(int strokWidth,boolean isAntiAlias);
    public MyImage creatSixStrokSideImage(int strokWidth,boolean isAntiAlias);
    public MyImage creatFiveStrokSideImage(int strokWidth,boolean isAntiAlias);
    public MyImage creatEightStrokSideImage(int strokWidth,boolean isAntiAlias);
    public MyImage creatCirculeStrokImage(int strokWidth,boolean isAntiAlias);

    public MyImage creatRoundImage(ImageCornerPersent persent,boolean isAntiAlias);
    public MyImage resizeImage(int width, int height, ImageQulity imageQulity,Bitmap.CompressFormat compresFormat);
}
