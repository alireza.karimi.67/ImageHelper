package myinterface;

import android.graphics.Bitmap;

/**
 * Created by alireza on 7/11/15.
 */
public interface InterfaceConvertor {
    public String convertToBase64();
    public Bitmap convertToBitmap();
    public byte[] convertToByte();
}
