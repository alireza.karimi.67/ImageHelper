package myinterface;

import android.graphics.Bitmap;

/**
 * Created by alireza on 7/11/15.
 */
public interface InterfaceObjectImage {
    public Bitmap toBitmap();
    public String toBase64();
    public byte[] toByte();
    public boolean saveImage(String path);
}
