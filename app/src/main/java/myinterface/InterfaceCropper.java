package myinterface;

import android.graphics.Bitmap;

/**
 * Created by alireza on 7/11/15.
 */
public interface InterfaceCropper {

    public Bitmap cropImageSide(Bitmap bitmap,boolean isAntiAlias);
    public Bitmap cropImageStrokSide(Bitmap bitmap,int strokWidth,boolean isAntiAlias);

}
