package ir.alirezakarimi.imagehelper.app;

import android.graphics.Bitmap;
import android.net.Uri;
import cropper.*;
import imageenum.ImageCornerPersent;
import imageenum.ImageQulity;
import myinterface.InterfaceMyImage;
import utility.ResizeImage;

/**
 * Created by alireza on 7/11/15.
 */
public class MyImage extends ObjectImage implements InterfaceMyImage{

    public MyImage(String imageBase64) {
        super(imageBase64);
    }

    public MyImage(byte[] imageByte) {
        super(imageByte);
    }

    public MyImage(Bitmap imageBitmap) {
        super(imageBitmap);
    }

    public MyImage(Uri imageFile) {
        super(imageFile);
    }

    @Override
    public MyImage creatTreeSideImage(boolean isAntiAlias) {

        ThreeSideImage cropper=new ThreeSideImage();
        Bitmap bitmapObject= cropper.cropImageSide(getImageBitmap(),isAntiAlias);
        MyImage object=new MyImage(bitmapObject);
        return object;
    }

    @Override
    public MyImage creatSixSideImage(boolean isAntiAlias) {
        SixSideImage cropper=new SixSideImage();
        Bitmap bitmapObject= cropper.cropImageSide(getImageBitmap(),isAntiAlias);
        MyImage object=new MyImage(bitmapObject);
        return object;
    }

    @Override
    public MyImage creatFiveSideImage(boolean isAntiAlias) {
        FiveSideImage cropper=new FiveSideImage();
        Bitmap bitmapObject= cropper.cropImageSide(getImageBitmap(),isAntiAlias);
        MyImage object=new MyImage(bitmapObject);
        return object;
    }

    @Override
    public MyImage creatEightSideImage(boolean isAntiAlias) {
        EightSideImage cropper=new EightSideImage();
        Bitmap bitmapObject= cropper.cropImageSide(getImageBitmap(),isAntiAlias);
        MyImage object=new MyImage(bitmapObject);
        return object;
    }

    @Override
    public MyImage creatCirculeImage(boolean isAntiAlias) {
        CirculeImage cropper=new CirculeImage();
        Bitmap bitmapObject= cropper.cropImageSide(getImageBitmap(),isAntiAlias);
        MyImage object=new MyImage(bitmapObject);
        return object;
    }

    @Override
    public MyImage creatTreeStrokSideImage(int strokWidth,boolean isAntiAlias) {

        ThreeSideImage cropper=new ThreeSideImage();
        Bitmap bitmapObject= cropper.cropImageStrokSide(getImageBitmap(),strokWidth,isAntiAlias);
        MyImage object=new MyImage(bitmapObject);
        return object;
    }

    @Override
    public MyImage creatSixStrokSideImage(int strokWidth,boolean isAntiAlias) {
        SixSideImage cropper=new SixSideImage();
        Bitmap bitmapObject= cropper.cropImageStrokSide(getImageBitmap(),strokWidth,isAntiAlias);
        MyImage object=new MyImage(bitmapObject);
        return object;
    }

    @Override
    public MyImage creatFiveStrokSideImage(int strokWidth,boolean isAntiAlias) {
        FiveSideImage cropper=new FiveSideImage();
        Bitmap bitmapObject= cropper.cropImageStrokSide(getImageBitmap(),strokWidth,isAntiAlias);
        MyImage object=new MyImage(bitmapObject);
        return object;
    }

    @Override
    public MyImage creatEightStrokSideImage(int strokWidth,boolean isAntiAlias) {
        EightSideImage cropper=new EightSideImage();
        Bitmap bitmapObject= cropper.cropImageStrokSide(getImageBitmap(),strokWidth,isAntiAlias);
        MyImage object=new MyImage(bitmapObject);
        return object;
    }

    @Override
    public MyImage creatCirculeStrokImage(int strokWidth,boolean isAntiAlias) {
        CirculeImage cropper=new CirculeImage();
        Bitmap bitmapObject= cropper.cropImageStrokSide(getImageBitmap(),strokWidth,isAntiAlias);
        MyImage object=new MyImage(bitmapObject);
        return object;
    }

    @Override
    public MyImage creatRoundImage(ImageCornerPersent persent,boolean isAntiAlias) {
        RoundCorner cropper=new RoundCorner();
        Bitmap bitmapObject= cropper.roundCorner(getImageBitmap(),persent,isAntiAlias);
        MyImage object=new MyImage(bitmapObject);
        return object;
    }

    @Override
    public MyImage resizeImage(int width, int height, ImageQulity imageQulity,Bitmap.CompressFormat compresFormat) {
        ResizeImage roundObject=new ResizeImage();
        byte[] byteImage=roundObject.creatResize(width,height,imageQulity,getImageBitmap(),compresFormat);
        MyImage object=new MyImage(byteImage);
        return object;
    }
}
