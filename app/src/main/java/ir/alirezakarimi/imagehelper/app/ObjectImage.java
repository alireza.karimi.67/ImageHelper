package ir.alirezakarimi.imagehelper.app;

import android.graphics.Bitmap;
import android.net.Uri;
import convertorNew.ConvertorBase64;
import convertorNew.ConvertorBitmap;
import convertorNew.ConvertorByte;
import convertorNew.ConvertorFile;
import imageenum.ImageQulity;
import myinterface.InterfaceObjectImage;
import utility.SaveImage;

/**
 * Created by alireza on 7/11/15.
 */
public class ObjectImage implements InterfaceObjectImage{
    private  String imageBase64;
    private  byte[] imageByte;
    private  Bitmap imageBitmap;
    private String imageFile;
    public ObjectImage(Uri imageFile)
    {
        this.imageFile=imageFile.getPath().replace("file://","/");
        ConvertorFile objectConvert=new ConvertorFile(this.imageFile);
        this.imageBitmap=objectConvert.convertToBitmap();
        this.imageByte=objectConvert.convertToByte(Bitmap.CompressFormat.JPEG,ImageQulity.MAX);
        this.imageBase64=objectConvert.convertToBase64(Bitmap.CompressFormat.JPEG,ImageQulity.MAX);
    }

    public ObjectImage(String imageBase64) {
        //convertorBase64 objectConvertBase64=new convertorBase64(imageBase64);
        ConvertorBase64 objectConvertor=new ConvertorBase64(imageBase64);
        this.imageBase64 = imageBase64;
        //this.imageBitmap=objectConvertBase64.convertToBitmap();
        //this.imageByte=objectConvertBase64.convertToByte();
        this.imageBitmap=objectConvertor.convertToBitmap();
        this.imageByte=objectConvertor.convertToByte(Bitmap.CompressFormat.JPEG,ImageQulity.MAX);

    }

    public ObjectImage(byte[] imageByte) {
        //convertorByte objectConvertorByte=new convertorByte(imageByte);
        //this.imageBitmap=objectConvertorByte.convertToBitmap();
        //this.imageBase64=objectConvertorByte.convertToBase64(Bitmap.CompressFormat.JPEG, ImageQulity.MAX);
        ConvertorByte objectConvertor=new ConvertorByte(imageByte);
        this.imageBitmap=objectConvertor.ConvertToBitmap();
        this.imageBase64=objectConvertor.ConvertToBase64(Bitmap.CompressFormat.JPEG, ImageQulity.MAX);
        this.imageByte = imageByte;
    }

    public ObjectImage(Bitmap imageBitmap) {
        //convertorBitmap objectConvertorBitma=new convertorBitmap(imageBitmap);
        //this.imageBase64=objectConvertorBitma.convertToBase64(Bitmap.CompressFormat.JPEG,ImageQulity.MAX);
        //this.imageByte=objectConvertorBitma.convertToByte(Bitmap.CompressFormat.JPEG, ImageQulity.MAX);
        ConvertorBitmap objectConvert=new ConvertorBitmap(imageBitmap);
        this.imageBitmap = imageBitmap;
        this.imageBase64=objectConvert.convertToBase64(Bitmap.CompressFormat.JPEG,ImageQulity.MAX);
        this.imageByte=objectConvert.convertToByte(Bitmap.CompressFormat.JPEG,ImageQulity.MAX);

    }

    public String getImageBase64() {
        return imageBase64;
    }

    public byte[] getImageByte() {
        return imageByte;
    }

    public Bitmap getImageBitmap() {
        return imageBitmap;
    }

    @Override
    public byte[] toByte()
    {
        return imageByte;
    }

    @Override
    public boolean saveImage(String path) {
        SaveImage objectSave=new SaveImage();
        return  objectSave.saveByteImage(getImageByte(),path);

    }

    @Override
    public Bitmap toBitmap(){
        return imageBitmap;
    }

    @Override
    public String toBase64() {
        return imageBase64;
    }


}
