package cropper;

import android.graphics.*;
import imageenum.ImageCornerPersent;

/**
 * Created by alireza on 7/1/15.
 */
public class RoundCorner {
    public Bitmap roundCorner(Bitmap bitmap, ImageCornerPersent persent,boolean isAntiAlias)
    {
        int color = 0xFFFFFFFF;
        Bitmap finalBitmap = bitmap;
        Bitmap outputBitmap = Bitmap.createBitmap(finalBitmap.getWidth(),
                finalBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(outputBitmap);
        Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, finalBitmap.getWidth(),
                finalBitmap.getHeight());

        Point point1 = new Point(persent.getValue(), 0);
        Point point2 = new Point(0, 0);
        Point point3 = new Point(0, persent.getValue());
        Point point4 = new Point(0,bitmap.getHeight()-persent.getValue());
        Point point5 = new Point(0,bitmap.getHeight());
        Point point6 = new Point(persent.getValue(),bitmap.getHeight());
        Point point7 = new Point(bitmap.getWidth()-persent.getValue(),bitmap.getHeight());
        Point point8 = new Point(bitmap.getWidth(),bitmap.getHeight());
        Point point9 = new Point(bitmap.getWidth(),bitmap.getHeight()-persent.getValue());
        Point point10 = new Point(bitmap.getWidth(),persent.getValue());
        Point point11 = new Point(bitmap.getWidth(),0);
        Point point12 = new Point(bitmap.getWidth()-persent.getValue(),0);
        Path path = new Path();
        path.moveTo(point1.x, point1.y);
        path.quadTo(point2.x,point2.y,point3.x,point3.y);
        path.lineTo(point4.x,point4.y);
        path.quadTo(point5.x,point5.y,point6.x,point6.y);
        path.lineTo(point7.x,point7.y);
        path.quadTo(point8.x,point8.y,point9.x,point9.y);
        path.lineTo(point10.x,point10.y);
        path.quadTo(point11.x,point11.y,point12.x,point12.y);
        path.lineTo(point1.x,point1.y);

        path.close();
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        paint.setAntiAlias(isAntiAlias);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeWidth(3.0f);


        canvas.drawPath(path, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(finalBitmap, rect, rect, paint);
        return outputBitmap;

    }

}
