package cropper;

import android.graphics.*;
import myinterface.InterfaceCropper;

/**
 * Created by alireza on 7/6/15.
 */
public class SixSideImage implements InterfaceCropper {
    @Override
    public Bitmap cropImageSide(Bitmap bitmap,boolean isAntiAlias) {
         int color=0xFFFFFFFF;
         Bitmap finalBitmap=bitmap;
         Bitmap output = Bitmap.createBitmap(finalBitmap.getWidth(),
                finalBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, finalBitmap.getWidth(),
                finalBitmap.getHeight());

        int partHeigh=bitmap.getHeight()/3;

        Point point1 = new Point(bitmap.getWidth()/2, 0);
        Point point2 = new Point(0, partHeigh);
        Point point3 = new Point(0, partHeigh*2);
        Point point4 = new Point(bitmap.getWidth()/2, partHeigh*3);
        Point point5 = new Point(bitmap.getWidth(), partHeigh*2);
        Point point6 = new Point(bitmap.getWidth(), partHeigh);

        Path path = new Path();
        path.moveTo(point1.x, point1.y);
        path.lineTo(point2.x, point2.y);
        path.lineTo(point3.x, point3.y);
        path.lineTo(point4.x, point4.y);
        path.lineTo(point5.x, point5.y);
        path.lineTo(point6.x, point6.y);
         path.lineTo(point1.x, point1.y);

        path.close();
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        paint.setAntiAlias(isAntiAlias);

        canvas.drawPath(path, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(finalBitmap, rect, rect, paint);

        return output;
    }

    @Override
    public Bitmap cropImageStrokSide(Bitmap bitmap,int strokWidth,boolean isAntiAlias) {
        int color=0xFFFFFFFF;
        Bitmap finalBitmap=bitmap;
        Bitmap output = Bitmap.createBitmap(finalBitmap.getWidth(),
                finalBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, finalBitmap.getWidth(),
                finalBitmap.getHeight());

        int partHeigh=bitmap.getHeight()/3;

        Point point1 = new Point(bitmap.getWidth()/2, 0);
        Point point2 = new Point(0, partHeigh);
        Point point3 = new Point(0, partHeigh*2);
        Point point4 = new Point(bitmap.getWidth()/2, partHeigh*3);
        Point point5 = new Point(bitmap.getWidth(), partHeigh*2);
        Point point6 = new Point(bitmap.getWidth(), partHeigh);

        Path path = new Path();
        path.moveTo(point1.x, point1.y);
        path.lineTo(point2.x, point2.y);
        path.lineTo(point3.x, point3.y);
        path.lineTo(point4.x, point4.y);
        path.lineTo(point5.x, point5.y);
        path.lineTo(point6.x, point6.y);
        path.lineTo(point1.x, point1.y);

        path.close();
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        paint.setAntiAlias(isAntiAlias);

        paint.setStrokeWidth(strokWidth);
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawPath(path, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(finalBitmap, rect, rect, paint);

        return output;
    }
}
