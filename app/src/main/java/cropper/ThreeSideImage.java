
package cropper;

import android.graphics.*;
import myinterface.InterfaceCropper;

/**
 * Created by alireza on 7/6/15.
 */
public class ThreeSideImage implements InterfaceCropper{
    @Override
    public Bitmap cropImageSide(Bitmap bitmap,boolean isAntiAlias) {
        int color = 0xFFFFFFFF;
        Bitmap finalBitmap = bitmap;
        Bitmap outputBitmap = Bitmap.createBitmap(finalBitmap.getWidth(),
                finalBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(outputBitmap);
        Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, finalBitmap.getWidth(),
                finalBitmap.getHeight());
        Point point1 = new Point(bitmap.getWidth() / 2, 0);
        Point point2 = new Point(0, bitmap.getHeight());
        Point point3 = new Point(bitmap.getWidth(), bitmap.getHeight());
        Path path = new Path();
        path.moveTo(point1.x, point1.y);
        path.lineTo(point2.x, point2.y);
        path.lineTo(point3.x, point3.y);
        path.lineTo(point1.x, point1.y);

        path.close();
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        paint.setAntiAlias(isAntiAlias);

        canvas.drawPath(path, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(finalBitmap, rect, rect, paint);
        return outputBitmap;
    }

    @Override
    public Bitmap cropImageStrokSide(Bitmap bitmap,int strokWidth,boolean isAntiAlias) {
        int color = 0xFFFFFFFF;
        Bitmap finalBitmap = bitmap;
        Bitmap outputBitmap = Bitmap.createBitmap(finalBitmap.getWidth(),
                finalBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(outputBitmap);
        Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, finalBitmap.getWidth(),
                finalBitmap.getHeight());
        Point point1 = new Point(bitmap.getWidth() / 2, 0);
        Point point2 = new Point(0, bitmap.getHeight());
        Point point3 = new Point(bitmap.getHeight(), bitmap.getHeight());
        Path path = new Path();
        path.moveTo(point1.x, point1.y);
        path.lineTo(point2.x, point2.y);
        path.lineTo(point3.x, point3.y);
        path.lineTo(point1.x, point1.y);

        path.close();
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        paint.setAntiAlias(isAntiAlias);

        paint.setStrokeWidth(strokWidth);
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawPath(path, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(finalBitmap, rect, rect, paint);
        return outputBitmap;
    }
}
