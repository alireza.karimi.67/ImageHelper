package cropper;

import android.graphics.*;
import myinterface.InterfaceCropper;

/**
 * Created by alireza on 7/6/15.
 */
public class CirculeImage implements InterfaceCropper{

    @Override
    public Bitmap cropImageSide(Bitmap bitmap,boolean isAntiAlias)
    {
        Bitmap returnBitmap= Bitmap.createBitmap(bitmap.getWidth(),bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas myCanvas=new Canvas(returnBitmap);
        int color=0xFFFFFFFF;
        Paint myPaint=new Paint();
        Rect myRect=new Rect(0,0,bitmap.getWidth(),bitmap.getHeight());
        myPaint.setAntiAlias(true);
        myPaint.setColor(color);
        myPaint.setAntiAlias(isAntiAlias);
        myCanvas.drawARGB(0,255,255,255);
        int radius=0;
        if (bitmap.getWidth()>bitmap.getHeight())
        {
            radius=bitmap.getHeight()/2;
        }else
        {
            radius=bitmap.getWidth()/2;
        }
        myCanvas.drawCircle(bitmap.getWidth()/2,bitmap.getHeight()/2,radius,myPaint);
        myPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        myCanvas.drawBitmap(bitmap,myRect,myRect,myPaint);
        return returnBitmap;
    }

    @Override
    public Bitmap cropImageStrokSide(Bitmap bitmap,int strokWidth,boolean isAntiAlias) {
        Bitmap returnBitmap= Bitmap.createBitmap(bitmap.getWidth(),bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas myCanvas=new Canvas(returnBitmap);
        int color=0xFFFFFFFF;
        Paint myPaint=new Paint();
        Rect myRect=new Rect(0,0,bitmap.getWidth(),bitmap.getHeight());
        myPaint.setAntiAlias(true);
        myPaint.setColor(color);
        myPaint.setAntiAlias(isAntiAlias);
        myPaint.setStrokeWidth(strokWidth);
        myCanvas.drawARGB(0,255,255,255);
        int radius=0;
        if (bitmap.getWidth()>bitmap.getHeight())
        {
            radius=bitmap.getHeight()/2;
        }else
        {
            radius=bitmap.getWidth()/2;
        }
        myCanvas.drawCircle(bitmap.getWidth()/2,bitmap.getHeight()/2,radius,myPaint);
        myPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        myCanvas.drawBitmap(bitmap,myRect,myRect,myPaint);
        return returnBitmap;
    }

}
