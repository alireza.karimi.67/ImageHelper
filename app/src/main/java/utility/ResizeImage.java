package utility;

import android.graphics.Bitmap;
import android.util.Log;
import imageenum.ImageQulity;

import java.io.ByteArrayOutputStream;

/**
 * Created by alireza on 7/2/15.
 */
public class ResizeImage{
    private final String TAG = "ResizeImage";

    public byte[] creatResize(int width, int height, ImageQulity imageQulity, Bitmap image,Bitmap.CompressFormat compresFormat) {
        try {

            Bitmap myImage = image;
            myImage = Bitmap.createScaledBitmap(myImage, width, height, false);
            ByteArrayOutputStream bArrOutStr = new ByteArrayOutputStream();
            myImage.compress(compresFormat, imageQulity.getValue(), bArrOutStr);
            return bArrOutStr.toByteArray();
        } catch (Exception ex) {
            Log.e(TAG, " An ERROR Occure -> " + ex.toString());
            return null;
        }

    }
}
