package utility;

import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by alireza on 7/2/15.
 */
public class SaveImage {
    private final String TAG="SaveImage";
    public boolean saveByteImage(byte[] imageByte,String path)
    {
        try{
            File myFile=new File(path);
            myFile.createNewFile();
            FileOutputStream fOutStr=new FileOutputStream(myFile);
            fOutStr.write(imageByte);
            fOutStr.close();
            return true;
        }catch (Exception ex)
        {
            Log.e(TAG,"An ERROR Occure -> "+ex.toString());
            return false;
        }
    }
}
